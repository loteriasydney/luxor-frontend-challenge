import styled from 'styled-components';

export const Container = styled.div`
  width: 100vw;
  height: 100vh;
  background-color: #484d57;
  display: flex;
`;

export const Wrapper = styled.div`
  flex: 1.5;
  height: 100vh;
  background-color: #2d2f36;
  position: relative;
`;

export const ListWrapper = styled.div`
  width: 100%;
  height: 100%;
  overflow-y: scroll;
  overflow-x: hidden;
  background: #2d2f36;
  padding: 35px;
  box-sizing: border-box;
  &::-webkit-scrollbar {
    display: none;
  }
`;

export const ListItem = styled.div`
  background: #3f414b;
  border-radius: 6px;
  display: flex;
  align-items: center;
  padding: 15px 20px;
  box-sizing: border-box;
  margin-bottom: 15px;
`;

export const ListImage = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  background-color: #ffffff;
  margin-right: 20px;
`;

export const ListId = styled.span`
  color: #f2c94c;
  font-weight: 400;
  margin-right: 20px;
`;

export const ListName = styled.span`
  color: #ededed;
  font-weight: 300;
`;

export default Container;
