import React, { useState } from 'react';
import { useQuery } from '@apollo/client';

import { POKEMON_LISTS_GET_QUERY } from '../graphql/queries/pokemon';
import PokemonLists from '../pokemon-lists';
import PokemonDetails from '../pokemon-details';
import Pagination from '../pagination';

import { Container, Wrapper } from './Dashboard.styles';

const Dashboard = () => {
  const { data: { pokemons = [], loading } = {} } = useQuery(POKEMON_LISTS_GET_QUERY, {
    variables: { first: 10 },
  });
  const [details, setDetails] = useState({});

  if (loading) {
    return null;
  }

  return (
    <Container>
      <Wrapper>
        <PokemonLists pokemons={pokemons} onClickSetDetails={setDetails} />
        <Pagination />
      </Wrapper>
      <PokemonDetails details={details} />
    </Container>
  );
};

export default Dashboard;
