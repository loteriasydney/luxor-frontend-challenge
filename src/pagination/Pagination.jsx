import React from 'react';

import { PaginationWrapper, Left, Right, Button } from './Pagination.styles';

const Pagination = () => (
  <PaginationWrapper>
    <Left>
      <Button>1</Button>
      <Button>2</Button>
      <Button>3</Button>
      <Button>4</Button>
    </Left>
    <Right>
      <Button>Previous</Button>
      <Button>Next</Button>
    </Right>
  </PaginationWrapper>
);

export default Pagination;
