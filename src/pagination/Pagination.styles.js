import styled from 'styled-components';

export const PaginationWrapper = styled.div`
  background-color: #1f1f1f;
  width: 100%;
  height: 70px;
  position: absolute;
  z-index: 2;
  bottom: 0;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 5px 8px;
  box-sizing: border-box;
`;

export const Left = styled.div``;
export const Right = styled.div`
  display: flex;
`;

export const Button = styled.button`
  cursor: pointer;
  background: #2d2f36;
  color: #ffffff;
  border: none;
  outline: none;
  height: 35px;
  margin-left: 8px;
  border-radius: 6px;
  padding: 0 15px;
`;
export default {};
