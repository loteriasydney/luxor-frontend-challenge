import { gql } from '@apollo/client';

export const POKEMON_LISTS_GET_QUERY = gql`
  query GetPokemonLists($first: Int!) {
    pokemons(first: $first) {
      id
      number
      name
      image
      classification
      weight {
        minimum
        maximum
      }
      height {
        minimum
        maximum
      }
    }
  }
`;

export default {};
