import React from 'react';
import { ApolloProvider } from '@apollo/client/react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import apolloClient from './apollo-client';
import Login from './login';
import Dashboard from './dashboard';

const App = () => (
  <ApolloProvider client={apolloClient}>
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Login} />
        <Route exact path="/dashboard" component={Dashboard} />
      </Switch>
    </BrowserRouter>
  </ApolloProvider>
);

export default App;
