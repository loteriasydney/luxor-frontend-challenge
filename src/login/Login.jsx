import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

import { Container, Wrapper, Form, ErrorMessage, Input, SubmitButton } from './Login.styles';

const Login = () => {
  const history = useHistory();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();

    if (username === 'admin' && password === 'admin') {
      history.push('/dashboard');
    } else {
      setError('Invalid Credentials');
      setPassword('');
    }
  };

  useEffect(() => {
    return () => {
      setUsername('');
      setPassword('');
      setError('');
    };
  }, []);

  return (
    <Container>
      <Wrapper>
        <Form onSubmit={handleSubmit}>
          {error && <ErrorMessage>{error}</ErrorMessage>}
          <Input
            placeholder="Username"
            onChange={(e) => setUsername(e.target.value)}
            value={username}
          />
          <Input
            type="password"
            placeholder="Password"
            onChange={(e) => setPassword(e.target.value)}
            value={password}
          />
          <SubmitButton disabled={!username || !password} type="submit">
            LOGIN
          </SubmitButton>
        </Form>
      </Wrapper>
    </Container>
  );
};

export default Login;
