import styled from 'styled-components';

export const Container = styled.div`
  background-color: #1c1d20;
  width: 100vw;
  height: 100vh;
  position: relative;
`;

export const Wrapper = styled.div`
  width: 400px;
  height: 400px;
  background-color: #2d2e36;
  border-radius: 8px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  padding: 0 50px;
  box-sizing: border-box;
`;

export const ErrorMessage = styled.div`
  background-color: #ff0000;
  color: #ffffff;
  width: 100%;
  padding: 20px;
  box-sizing: border-box;
  margin-bottom: 28px;
  border-radius: 6px;
`;

export const Form = styled.form`
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const Input = styled.input`
  color: #fff;
  width: 100%;
  height: 50px;
  font-weight: 300;
  font-size: 18px;
  padding: 12px;
  background-color: #3f414b;
  box-sizing: border-box;
  outline: none;
  border: none;
  border-radius: 6px;
  margin-bottom: 28px;
  border: 1px solid transparent;
  &::placeholder {
    color: #8b8b8b;
  }
  &:focus {
    border: 1px solid #f2c94c;
  }
`;

export const SubmitButton = styled.button`
  background-color: #f2c94c;
  width: 100%;
  height: 50px;
  border: none;
  outline: none;
  border-radius: 6px;
  color: #ffffff;
  font-size: 18px;
  cursor: pointer;
  &:disabled {
    opacity: 0.5;
    cursor: not-allowed;
  }
`;

export default {};
