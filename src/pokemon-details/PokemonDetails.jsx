import React from 'react';
import PropTypes from 'prop-types';

import {
  Wrapper,
  Header,
  Name,
  Number,
  Body,
  Image,
  Type,
  Size,
  ImageWrapper,
} from './PokemonDetails.styles';

const PokemonDetails = ({ details }) => {
  return (
    <Wrapper>
      {Object.values(details).length > 0 && (
        <>
          <Header>
            <Name>{details.name}</Name>
            <Number>#{details.number}</Number>
          </Header>
          <Body>
            <ImageWrapper>
              <Image src={details.image} />
            </ImageWrapper>
            <Type>Type: {details.classification}</Type>
            <Size bgColor="#115173">
              Weight: {details.weight.minimum} - {details.weight.maximum}
            </Size>
            <Size bgColor="#a0204c">
              Height: {details.height.minimum} - {details.height.maximum}
            </Size>
          </Body>
        </>
      )}
    </Wrapper>
  );
};

PokemonDetails.defaultProps = {
  details: {},
};

PokemonDetails.propTypes = {
  details: PropTypes.shape({
    name: PropTypes.string,
    number: PropTypes.string,
    image: PropTypes.string,
    classification: PropTypes.string,
    weight: PropTypes.shape({
      minimum: PropTypes.string,
      maximum: PropTypes.string,
    }),
    height: PropTypes.shape({
      minimum: PropTypes.string,
      maximum: PropTypes.string,
    }),
  }),
};

export default PokemonDetails;
