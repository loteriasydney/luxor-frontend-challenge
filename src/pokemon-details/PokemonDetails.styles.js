import styled from 'styled-components';

export const Wrapper = styled.div`
  flex: 3;
  height: 100vh;
  background-color: #3b3e46;
  position: relative;
`;

export const Header = styled.div`
  width: 100%;
  height: 70px;
  border-bottom: 2px solid #2d2f36;
  display: flex;
  align-items: center;
  padding: 0 35px;
  box-sizing: border-box;
  justify-content: space-between;
`;

export const Name = styled.span`
  font-size: 20px;
  color: #ffffff;
`;

export const Number = styled.span`
  color: #f2c94c;
  font-size: 20px;
`;

export const Body = styled.div`
  padding: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

export const ImageWrapper = styled.div`
  overflow: hidden;
  box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px;
  width: 300px;
  height: 300px;
  border-radius: 8px;
`;

export const Image = styled.img`
  height: 100%;
  width: 100%;
`;

export const Type = styled.span`
  text-align: center;
  width: 270px;
  margin-top: 20px;
  background-color: #f2c94c;
  color: #ffffff;
  font-weight: 700;
  padding: 15px;
  font-size: 18px;
  border-radius: 8px;
  margin-bottom: 20px;
`;

export const Size = styled.span`
  background-color: ${(props) => props.bgColor};
  color: #ffffff;
  font-weight: 700;
  padding: 15px;
  font-size: 18px;
  border-radius: 8px;
  width: 270px;
  text-align: center;
  margin-bottom: 20px;
`;

export default {};
