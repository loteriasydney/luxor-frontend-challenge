import React from 'react';
import ReactDOM from 'react-dom';
import WebFont from 'webfontloader';

import App from './App';
import reportWebVitals from './reportWebVitals';
import './index.css';

WebFont.load({
  custom: {
    families: ['Roboto:300,400,700', 'sans-serif'],
    urls: ['https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap'],
  },
});

ReactDOM.render(<App />, document.getElementById('root'));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
