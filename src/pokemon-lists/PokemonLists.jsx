import React from 'react';
import PropTypes from 'prop-types';

import { ListWrapper, ListItem, ListImage, ListId, ListName } from './PokemonLists.styles';

const PokemonLists = ({ pokemons, onClickSetDetails }) => {
  return (
    <ListWrapper>
      {pokemons.map((pokemon) => (
        <ListItem key={pokemon.id} onClick={() => onClickSetDetails(pokemon)}>
          <ListImage src={pokemon.image} alt={pokemon.name} title={pokemon.name} />
          <ListId>{pokemon.number}</ListId>
          <ListName>{pokemon.name}</ListName>
        </ListItem>
      ))}
    </ListWrapper>
  );
};

PokemonLists.defaultProps = {
  pokemons: [],
};

PokemonLists.propTypes = {
  pokemons: PropTypes.arrayOf(PropTypes.object),
  onClickSetDetails: PropTypes.func.isRequired,
};

export default PokemonLists;
