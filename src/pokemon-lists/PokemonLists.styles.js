import styled from 'styled-components';

export const ListWrapper = styled.div`
  width: 100%;
  height: 100%;
  overflow-y: scroll;
  overflow-x: hidden;
  background: #2d2f36;
  padding: 40px 35px 75px 35px;
  box-sizing: border-box;
  &::-webkit-scrollbar {
    display: none;
  }
`;

export const ListItem = styled.div`
  cursor: pointer;
  background: #3f414b;
  border-radius: 6px;
  display: flex;
  align-items: center;
  padding: 15px 20px;
  box-sizing: border-box;
  margin-bottom: 15px;
  &:hover {
    background: #25272d;
  }
`;

export const ListImage = styled.img`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  background-color: #ffffff;
  margin-right: 20px;
`;

export const ListId = styled.span`
  color: #f2c94c;
  font-weight: 400;
  margin-right: 20px;
`;

export const ListName = styled.span`
  color: #ededed;
  font-weight: 300;
`;
